const {createApp} = Vue;
createApp
({
    data()
    {
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './Images/lua.jpg',
                './Images/sol.jpg',
            ],
            
            imagensInternet:
            [
                'https://www.encontracarros.com.br/upload/mclaren/mclaren-p1-gtr-senna_1280.jpg',

                'https://mclaren.scene7.com/is/image/mclaren/McLaren-P1-Hero-1920x1080:crop-16x9?wid=1980&hei=1114',

                'https://static.feber.se/article_images/55/06/47/550647.jpeg',
            ]

        };//fim return
    },//fim data

    computed:
    {
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },//fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }//fim randomInternet
    },//fim computed

    methods:
    {
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length);
        },//fim methods
    },

}).mount("#app");