//Processamento dos dados do formulário "index

//Acessando os elemento formulário do html
const formulario = document.getElementById("formulario1");

//adiciona um ouvinto de eventos a um elemneto HTML
formulario.addEventListener("submit", function(evento)
{
    evento.preventDefault();/*previne o compartamento padrão de um elemento HTML em resposta a um eveneto*/

    //constantes para tratar os dados recebidos dos elementos do formulário
    const nome = document.getElementById ("nome").value;
    const email = document.getElementById ("email").value;

    //Exibe um alerta com os dados coletados
    //alert(`nome: ${nome} --- E-mail: ${email}`);

    const updateResultado = document.getElementById("resultado");

    updateResultado.value = `Nome: ${nome} ---- E-mail: ${email}`;

    updateResultado.style.width = updateResultado.scrollwidth + "px";

});
