const {createApp} = Vue; 

createApp({
    data(){
        return{
            numero:0,
        };
    },

    methods:{
        addValor:function(){
            this.numero++;
        }
    }

}).mount("#app");
